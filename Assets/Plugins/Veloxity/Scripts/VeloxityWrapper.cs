﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class VeloxityWrapper : MonoBehaviour
{

    public static Veloxity.AuthorizationSucceedListener authorizationSucceedListener = null;
    public static Veloxity.AuthorizationFailedListener authorizationFailedListener = null;

    [DllImport("__Internal")]
    public static extern void setLicenseKey(string licenseKey);

    [DllImport("__Internal")]
    public static extern void setWebServiceUrl(string webServiceUrl);


    [DllImport("__Internal")]
    public static extern void setAuthorizationMenu(string title, string message, string acceptTitle, string denyTitle);

    [DllImport("__Internal")]
    public static extern void optIn();

    [DllImport("__Internal")]
    public static extern void optOut();


    [DllImport("__Internal")]
    public static extern void registerDeviceToken(byte[] token);


    [DllImport("__Internal")]
    public static extern void start();


    [DllImport("__Internal")]
    public static extern bool getServiceStatus();


    [DllImport("__Internal")]
    public static extern string getDeviceId();


    [DllImport("__Internal")]
    public static extern void setUserIdentifier(string identifier);


    [DllImport("__Internal")]
    public static extern void sendCustomData(string data);


    [DllImport("__Internal")]
    public static extern void startBackgroundTransactionWithUserInfo(string data);


    [DllImport("__Internal")]
    public static extern void setAuthorizationFailedListener();


    [DllImport("__Internal")]
    public static extern void setAuthorizationSucceedListener();


    public static void _setLicenseKey(string licenseKey)
    {
        // Call plugin only when running on real device
        if (Application.platform != RuntimePlatform.OSXEditor)
            setLicenseKey(licenseKey);
    }

    public static void _setWebServiceUrl(string webServiceUrl)
    {
        // Call plugin only when running on real device
        if (Application.platform != RuntimePlatform.OSXEditor)
            setWebServiceUrl(webServiceUrl);
    }

    public static void _setAuthorizationMenu(string title, string message, string acceptTitle, string denyTitle)
    {
        // Call plugin only when running on real device
        if (Application.platform != RuntimePlatform.OSXEditor)
            setAuthorizationMenu(title, message, acceptTitle, denyTitle);
    }

    public static void _optIn()
    {
        // Call plugin only when running on real device
        if (Application.platform != RuntimePlatform.OSXEditor)
            optIn();
    }

    public static void _optOut()
    {
        // Call plugin only when running on real device
        if (Application.platform != RuntimePlatform.OSXEditor)
            optOut();
    }

    public static void _registerDeviceToken(byte[] token)
    {
        // Call plugin only when running on real device
        if (Application.platform != RuntimePlatform.OSXEditor)
            registerDeviceToken(token);
    }

    public static void _setAuthorizationSucceedListener(Veloxity.AuthorizationSucceedListener listener)
    {
        authorizationSucceedListener = listener;
        setAuthorizationSucceedListener();
    }

    public static void _setAuthorizationFailedListener(Veloxity.AuthorizationFailedListener listener)
    {
        authorizationFailedListener = listener;
        setAuthorizationFailedListener();
    }

    public static void _start()
    {
        start();
    }

    public static bool _getServiceStatus()
    {
        return getServiceStatus();
    }

    public static string _getDeviceId()
    {
        return getDeviceId();
    }

    public static void _setUserIdentifier(string identifier)
    {
        // Call plugin only when running on real device
        if (Application.platform != RuntimePlatform.OSXEditor)
            setUserIdentifier(identifier);
    }

    public static void _sendCustomData(string data)
    {
        // Call plugin only when running on real device
        if (Application.platform != RuntimePlatform.OSXEditor)
            sendCustomData(data);
    }

    public static void _startBackgroundTransactionWithUserInfo(string data)
    {
        // Call plugin only when running on real device
        if (Application.platform != RuntimePlatform.OSXEditor)
            startBackgroundTransactionWithUserInfo(data);
    }

}
