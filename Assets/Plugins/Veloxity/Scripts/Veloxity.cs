using System;
using UnityEngine;
using System.Collections;

// We need this one for importing our IOS functions
using System.Runtime.InteropServices;
using System.Collections.Generic;
using UnityEngine.Events;
using MiniJSON;

#if UNITY_IPHONE && !UNITY_EDITOR
using NotificationServices = UnityEngine.iOS.NotificationServices;
using NotificationType = UnityEngine.iOS.NotificationType;
#endif

public class Veloxity : MonoBehaviour
{
    public const string UNITY_GAME_OBJECT_NAME = "VeloxityUnityPlugin";
    public UnityEvent authorizationSucceedEvent;
    public UnityEvent authorizationFailedEvent;
    public UnityEvent authorizationCompletedEvent;
    private string licenseKey = "";
    public string iOSLicenseKey;
    public string androidLicenseKey;
    public int priority;
    public string gcmSenderID;
    public string dialogMessage;
    public string dialogTitle;
    public string acceptTitle;
    public string denyTitle;
    public string webServiceEndpoint;

    public delegate void AuthorizationSucceedListener();

    public delegate void AuthorizationFailedListener();

    public delegate void AuthorizationCompletedListener();

    private static Veloxity _instance;

    public static Veloxity Instance { get { return _instance; } }

#if UNITY_IPHONE && !UNITY_EDITOR
    private bool tokenIsSent = false;
#endif


    private void Awake()
    {
#if UNITY_IPHONE && !UNITY_EDITOR
            gameObject.name = UNITY_GAME_OBJECT_NAME;
#endif
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            GameObject UnityMainThreadDispatcher = Instantiate(new GameObject());
            UnityMainThreadDispatcher.AddComponent<UnityMainThreadDispatcher>();
            DontDestroyOnLoad(UnityMainThreadDispatcher);
            DontDestroyOnLoad(this);
        }

#if UNITY_IPHONE && !UNITY_EDITOR
   this.licenseKey = this.iOSLicenseKey;
#endif
#if UNITY_ANDROID && !UNITY_EDITOR
    this.licenseKey = this.androidLicenseKey;
#endif
    }

    void Update()
    {
#if UNITY_IPHONE && !UNITY_EDITOR
        byte[] token = NotificationServices.deviceToken;
        if (!tokenIsSent && token != null)
        {
            // send token to a provider
            Veloxity.Instance.registerDeviceToken(token);
            tokenIsSent = true;
        }
#endif
    }

    public void veloxityStart()
    {
        new Builder().setPriority(priority).setLicenseKey(licenseKey).setAuthorizationSucceedListener(authorizationSucceedEvent).setAuthorizationFailedListener(authorizationFailedEvent).setDialogMessage(dialogMessage).setDialogTitle(dialogTitle).setAcceptTitle(acceptTitle).setDenyTitle(denyTitle).setGCMSenderID(gcmSenderID).setWebServiceEndpoint(webServiceEndpoint).start();
    }


#if UNITY_ANDROID && !UNITY_EDITOR
    private Veloxity initialize(AndroidJavaObject veloxityOptions)
    {
    JavaObjectWrapper.cls_Veloxity.CallStatic("initialize", veloxityOptions);
    return this;
    }
#endif

    public void optIn()
    {
        new Builder().setPriority(priority).setLicenseKey(licenseKey).setAuthorizationSucceedListener(authorizationSucceedEvent).setAuthorizationFailedListener(authorizationFailedEvent).setDialogMessage(dialogMessage).setDialogTitle(dialogTitle).setAcceptTitle(acceptTitle).setDenyTitle(denyTitle).setGCMSenderID(gcmSenderID).setWebServiceEndpoint(webServiceEndpoint).optIn();
    }

    public void optOut()
    {
#if UNITY_IPHONE && !UNITY_EDITOR
        VeloxityWrapper._optOut();
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
        JavaObjectWrapper.cls_Veloxity.CallStatic("optOut", JavaObjectWrapper.activityContext);
#endif
    }

    public void registerDeviceToken(byte[] token)
    {
#if UNITY_IPHONE && !UNITY_EDITOR
        VeloxityWrapper._registerDeviceToken(token);
#endif
    }

    public string getDeviceId()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        return JavaObjectWrapper.cls_Veloxity.CallStatic<string>("getDeviceId", JavaObjectWrapper.activityContext);

#elif UNITY_IPHONE
        return VeloxityWrapper._getDeviceId();
#else
        return "";
#endif
    }

    public bool isInVeloxityProcess()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        return JavaObjectWrapper.cls_Veloxity.CallStatic<bool>("isInVeloxityProcess", JavaObjectWrapper.activityContext);
#else
        return false;
#endif
    }

    public void setUserIdentifier(string identifier)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        JavaObjectWrapper.cls_Veloxity.CallStatic("sendMsisdn", JavaObjectWrapper.activityContext, identifier);
#endif

#if UNITY_IPHONE && !UNITY_EDITOR
        VeloxityWrapper._setUserIdentifier(identifier);
#endif
    }

    public void registerLifeCycleCallbacks()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        AndroidJavaClass app = new AndroidJavaClass("android.app.ActivityThread");
        JavaObjectWrapper.cls_Veloxity.CallStatic("registerLifeCycleCallbacks", app.CallStatic<AndroidJavaObject>("currentApplication"));
#endif
    }

    public bool getServiceStatus()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        return JavaObjectWrapper.cls_Veloxity.CallStatic<bool>("isServiceRunning", JavaObjectWrapper.activityContext);
#elif UNITY_IPHONE && !UNITY_EDITOR
        return VeloxityWrapper._getServiceStatus();
#else
        return false;
#endif
    }

    public void sendDataUsageStatus(string webServiceEndpoint, bool isDataUsageAccepted)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        JavaObjectWrapper.cls_Veloxity.CallStatic("sendDataUsageStatus", JavaObjectWrapper.activityContext, webServiceEndpoint, isDataUsageAccepted);
#endif
    }

    public void sendCustomData(object jsonObject)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        AndroidJavaObject cls_JSONObject = new AndroidJavaObject("org.json.JSONObject", MiniJSON.Json.Serialize(jsonObject));
        JavaObjectWrapper.cls_Veloxity.CallStatic("sendCustomData", JavaObjectWrapper.activityContext, cls_JSONObject);
#endif

#if UNITY_IPHONE && !UNITY_EDITOR
        VeloxityWrapper._sendCustomData(MiniJSON.Json.Serialize(jsonObject));
#endif
    }

    public void startBackgroundTransactionWithUserInfo(object jsonObject)
    {
#if UNITY_IPHONE && !UNITY_EDITOR
        VeloxityWrapper._startBackgroundTransactionWithUserInfo(MiniJSON.Json.Serialize(jsonObject));
#endif
    }

    public void vlxAuthorizationDidSucceed()
    {
        authorizationSucceedEvent.Invoke();
    }

    public void vlxAuthorizationDidFailed()
    {
        authorizationFailedEvent.Invoke();
    }

#if UNITY_ANDROID && !UNITY_EDITOR
    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus == true)
        {
            JavaObjectWrapper.cls_Veloxity.CallStatic("onPause", JavaObjectWrapper.activityContext);
        }
        else{
            JavaObjectWrapper.cls_Veloxity.CallStatic("onResume", JavaObjectWrapper.activityContext);
        }
    }
#endif

    private class Builder
    {
        public string title = "";
        public string message = "";
        public string accept = "";
        public string deny = "";

        public Builder setLicenseKey(String licenseKey)
        {
#if UNITY_IPHONE
            VeloxityWrapper._setLicenseKey(licenseKey);
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
            JavaObjectWrapper.cls_VeloxityOptionsBuilder.Call<AndroidJavaObject>("setLicenseKey", licenseKey);
#endif
            return this;
        }

        public Builder setPriority(int priority)
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            JavaObjectWrapper.cls_VeloxityOptionsBuilder.Call<AndroidJavaObject>("setPriority", priority);
#endif
            return this;
        }

        public Builder setWebServiceEndpoint(String webServiceEndpoint)
        {
#if UNITY_IPHONE
            VeloxityWrapper._setWebServiceUrl(webServiceEndpoint);
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
            JavaObjectWrapper.cls_VeloxityOptionsBuilder.Call<AndroidJavaObject>("setWebServiceEndpoint", webServiceEndpoint);
#endif
            return this;
        }

        public Builder setAuthorizationSucceedListener(UnityEvent listener)
        {
            if (listener.GetPersistentEventCount() <= 0)
                return this;
#if UNITY_IPHONE && !UNITY_EDITOR
            VeloxityWrapper._setAuthorizationSucceedListener(Veloxity.Instance.vlxAuthorizationDidSucceed);
#endif
            return this;
        }

        public Builder setAuthorizationFailedListener(UnityEvent listener)
        {
            if (listener.GetPersistentEventCount() <= 0)
                return this;

#if UNITY_IPHONE && !UNITY_EDITOR
            VeloxityWrapper._setAuthorizationFailedListener(Veloxity.Instance.vlxAuthorizationDidFailed);
#endif
            return this;
        }

        public Builder setGCMSenderID(String gcmSenderId)
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            JavaObjectWrapper.cls_VeloxityOptionsBuilder.Call<AndroidJavaObject>("setGCMSenderID", gcmSenderId);
#endif
            return this;
        }

        public Builder setDialogTitle(String title)
        {
#if UNITY_IPHONE && !UNITY_EDITOR
            this.title = title;
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
            JavaObjectWrapper.cls_VeloxityOptionsBuilder.Call<AndroidJavaObject>("setDialogTitle", title);
#endif
            return this;
        }

        public Builder setDialogMessage(String message)
        {
#if UNITY_IPHONE && !UNITY_EDITOR
            this.message = message;
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
            JavaObjectWrapper.cls_VeloxityOptionsBuilder.Call<AndroidJavaObject>("setDialogMessage", message);
#endif
            return this;
        }

        public Builder setAcceptTitle(String message)
        {
#if UNITY_IPHONE && !UNITY_EDITOR
            this.accept = message;
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
            JavaObjectWrapper.cls_VeloxityOptionsBuilder.Call<AndroidJavaObject>("setDialogPositive", message);
#endif
            return this;
        }

        public Builder setDenyTitle(String message)
        {
#if UNITY_IPHONE && !UNITY_EDITOR
            this.deny = message;
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
            JavaObjectWrapper.cls_VeloxityOptionsBuilder.Call<AndroidJavaObject>("setDialogNegative", message);
#endif
            return this;
        }

        public Veloxity optIn()
        {
#if UNITY_IPHONE && !UNITY_EDITOR
            VeloxityWrapper._optIn();
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
            if (Veloxity.Instance.authorizationSucceedEvent.GetPersistentEventCount() > 0 || Veloxity.Instance.authorizationFailedEvent.GetPersistentEventCount() > 0 || Veloxity.Instance.authorizationCompletedEvent.GetPersistentEventCount() > 0)
            {
                JavaObjectWrapper.cls_VeloxityOptionsBuilder.Call<AndroidJavaObject>("setListener", new DataUsageListener());
            }
            JavaObjectWrapper.cls_Veloxity.CallStatic("optIn", JavaObjectWrapper.cls_VeloxityOptionsBuilder.Call<AndroidJavaObject>("build"));
#endif
            return Veloxity.Instance;
        }

        public Veloxity start()
        {
#if UNITY_IPHONE && !UNITY_EDITOR
            VeloxityWrapper._setAuthorizationMenu(title, message, accept, deny);
            VeloxityWrapper._start();
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
            if (Veloxity.Instance.authorizationSucceedEvent.GetPersistentEventCount() > 0 || Veloxity.Instance.authorizationFailedEvent.GetPersistentEventCount() > 0 || Veloxity.Instance.authorizationCompletedEvent.GetPersistentEventCount() > 0)
            {
                JavaObjectWrapper.cls_VeloxityOptionsBuilder.Call<AndroidJavaObject>("setListener", new DataUsageListener());
            }
            Veloxity.Instance.initialize(JavaObjectWrapper.cls_VeloxityOptionsBuilder.Call<AndroidJavaObject>("build"));
#endif
            return Veloxity.Instance;
        }
    }

    public class DataUsageListener : AndroidJavaProxy
    {
        public DataUsageListener()
            : base("net.veloxity.publicapi.DataUsageListener")
        {
        }

        public void onDataUsageResult(bool isServiceStarted)
        {
            if (isServiceStarted)
            {
                UnityMainThreadDispatcher.Instance().Enqueue(() => Veloxity.Instance.authorizationSucceedEvent.Invoke());
            }
            else
            {
                UnityMainThreadDispatcher.Instance().Enqueue(() => Veloxity.Instance.authorizationFailedEvent.Invoke());
            }
        }

        public void onCompleted()
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => Veloxity.Instance.authorizationCompletedEvent.Invoke());
        }
    }
}



