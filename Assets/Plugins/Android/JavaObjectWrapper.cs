﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JavaObjectWrapper : MonoBehaviour
{
#if UNITY_ANDROID && !UNITY_EDITOR 
    public static AndroidJavaClass activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

    public static AndroidJavaObject activityContext = activity.GetStatic<AndroidJavaObject>("currentActivity");

    public static AndroidJavaObject cls_Veloxity = new AndroidJavaObject("net.veloxity.sdk.Veloxity");

    public static AndroidJavaObject cls_VeloxityOptionsBuilder = new AndroidJavaObject("net.veloxity.sdk.VeloxityOptions$Builder", activityContext);
#endif
}
