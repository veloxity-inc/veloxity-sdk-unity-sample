//
//  Veloxity.h
//  V
//
//  Created by Mert Güneş on 18/04/14.
//  Copyright (c) 2014 Mert Güneş. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^CompletionHandlerType)();

@protocol VeloxityDelegate <NSObject>

- (void)vlxAuthorizationDidSucceed;

- (void)vlxAuthorizationDidFailed;

@end

@interface Veloxity : NSObject

@property (nonatomic) BOOL isRunning;

@property (nonatomic, strong) id<VeloxityDelegate> delegate;

/**
 * Create a Veloxity object if not created before and assign it as the shared instance of the class.
 *
 * @return shared instance of Veloxity class.
 */
+ (Veloxity*)sharedInstance;

/**
 * Set the license key
 */
- (void)setLicenseKey:(NSString*)_licenseKey;

/**
 * Set the user Identifier(For eg: user's msisdn or user's email address)
 */
- (void)setUserIdentifier:(NSString*)identifier;

/**
 * Send custom data to the Cloud
 */
- (void)sendCustomData:(NSDictionary*)jsonContent;

/**
 * Set the parent view to show promotional popups
 */
- (void)setParentView:(UIView*)parentView DEPRECATED_ATTRIBUTE;

/**
 * Set the web service url
 */
- (void)setWebServiceUrl:(NSString*)webServiceUrl;

/**
 * Set the data usage authorization menu information
 */
- (void)setAuthorizationMenu:(NSString*)title withMessage:(NSString*)message andAcceptTitle:(NSString*)acceptTitle andDenyTitle:(NSString*)denyTitle;

/**
 * Start Veloxity
 */
- (void)start;

/**
 * OptIn Veloxity
 */
- (void)optIn;

/**
 * OptOut Veloxity
 */
- (void)optOut;

/**
 * Status of Veloxity
 */
- (BOOL)serviceStatus;

/**
 * Starts the synthetic transaction in the background with userInfo that sent with a silent push
 * Completion handler is required for push notifications that are handled in background
 */
- (void)startBackgroundTransactionWithUserInfo:(NSDictionary*)userInfo;

/**
 * Sends the device token to the server in order to register for silent push notifications
 */
- (void)registerDeviceToken:(NSData*)deviceToken;

/**
 * Sends the device token to the server in order to register for silent push notifications
 */
- (NSString*)getDeviceId;

- (void)getSettingsFromCloud;

- (void)continueStartAfterSettingsReceived;

- (void)notifyDataUsageStatusWithServiceUrl:(NSString*)webServiceUrl isAllowed:(BOOL)isAllowed;

- (void)setAuthroizationFailListener:(void (^)())listener;

- (void)setAuthroizationSucceedListener:(void (^)())listener;

@end
