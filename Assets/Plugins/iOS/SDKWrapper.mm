#include "SDKWrapper.h"
#import "Veloxity.h"

NSString* CreateNSString(const char* string)
{
    if (string)
        return [NSString stringWithUTF8String:string];
    else
        return [NSString stringWithUTF8String:""];
}

NSData* dataFromString(const char* string)
{
    NSMutableData* d = [[NSMutableData alloc] init];
    [d appendBytes:string length:strlen(string)];
    return d;
}

char* cStringCopy(const char* string)
{
    if (string == NULL)
        return NULL;

    char* res = (char*)malloc(strlen(string) + 1);
    strcpy(res, string);

    return res;
}

const char* dictionaryToJsonChar(NSDictionary* dictionaryToConvert) {
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictionaryToConvert options:0 error:nil];
    NSString* jsonRequestData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    return jsonRequestData.UTF8String;
}

NSDictionary* CreateNSDictionary(const char* data)
{
    NSString *stringData = CreateNSString(data);
    NSError* jsonError;
    NSData* objectData = [stringData dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:objectData
                                                         options:NSJSONReadingMutableContainers
                                                           error:&jsonError];
    if (!jsonError)
        return json;
    else {
        return @{};
    }
}

extern "C" {
void setLicenseKey(const char* key)
{

    [[Veloxity sharedInstance] setLicenseKey:CreateNSString(key)];
}
}

extern "C" {
void setWebServiceUrl(const char* webServiceUrl)
{
    [[Veloxity sharedInstance] setWebServiceUrl:CreateNSString(webServiceUrl)];
}
}

extern "C" {
void setAuthorizationMenu(const char* title, const char* message, const char* acceptTitle, const char* denyTitle)
{
    [[Veloxity sharedInstance] setAuthorizationMenu:CreateNSString(title) withMessage:CreateNSString(message) andAcceptTitle:CreateNSString(acceptTitle) andDenyTitle:CreateNSString(denyTitle)];
}
}

extern "C" {
void registerDeviceToken(const char* token)
{
    [[Veloxity sharedInstance] registerDeviceToken:dataFromString(token)];
}
}

extern "C" {
void start()
{   
    [[Veloxity sharedInstance] start];
}
}

extern "C" {
void setAuthorizationSucceedListener()
{
   
    [[Veloxity sharedInstance] setAuthroizationSucceedListener:^() {
        UnitySendMessage("VeloxityUnityPlugin", "vlxAuthorizationDidSucceed", "");
    }];
}
}

extern "C" {
void setAuthorizationFailedListener()
{
   
    [[Veloxity sharedInstance] setAuthroizationFailListener:^() {
        UnitySendMessage("VeloxityUnityPlugin", "vlxAuthorizationDidFailed", "");
    }];
}
}
extern "C" {
void optIn()
{
   
    [[Veloxity sharedInstance] optIn];
}
}

extern "C" {
void optOut()
{
    [[Veloxity sharedInstance] optOut];
}
}

extern "C" {
bool getServiceStatus()
{
    return [[Veloxity sharedInstance] serviceStatus];
}
}

extern "C" {
const char* getDeviceId()
{
    return cStringCopy([[[Veloxity sharedInstance] getDeviceId] UTF8String]);
}
}

extern "C" {
void setUserIdentifier(const char* identifier)
{
    [[Veloxity sharedInstance] setUserIdentifier:CreateNSString(identifier)];
}
}

extern "C" {
    void sendCustomData(const char* data)
    {
        [[Veloxity sharedInstance] sendCustomData:CreateNSDictionary(data)];
    }
}

extern "C" {
    void startBackgroundTransactionWithUserInfo(const char* data)
    {
        [[Veloxity sharedInstance] startBackgroundTransactionWithUserInfo:CreateNSDictionary(data)];
    }
}
