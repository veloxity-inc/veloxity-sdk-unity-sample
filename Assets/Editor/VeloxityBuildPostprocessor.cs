﻿#if (UNITY_5 || UNITY_2017_2_OR_NEWER) && UNITY_IPHONE
using UnityEngine;
using UnityEditor;
using UnityEditor.iOS.Xcode;
using UnityEditor.Callbacks;

public class VeloxityBuildPostprocessor
{
    [PostProcessBuild(700)]
    public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
    {
        if (target != BuildTarget.iOS)
        {
            return;
        }
        string projPath = pathToBuiltProject + "/Unity-iPhone.xcodeproj/project.pbxproj";
        PBXProject proj = new PBXProject();
        proj.ReadFromString(System.IO.File.ReadAllText(projPath));
        string targetGUID = proj.TargetGuidByName("Unity-iPhone");
        proj.AddBuildProperty(targetGUID, "OTHER_LDFLAGS", "-ObjC");
        proj.AddFileToBuild(targetGUID, proj.AddFile("Frameworks/libz.tbd",
                "Frameworks/libz.tbd", PBXSourceTree.Source));
                proj.AddFileToBuild(targetGUID, proj.AddFile("Frameworks/libsqlite3.0.tbd",
                "Frameworks/libsqlite3.0.tbd", PBXSourceTree.Source));
        System.IO.File.WriteAllText(projPath, proj.WriteToString());
    }
}
#endif