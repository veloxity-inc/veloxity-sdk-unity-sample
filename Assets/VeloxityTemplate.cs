﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MiniJSON;

#if UNITY_IPHONE && !UNITY_EDITOR
using NotificationServices = UnityEngine.iOS.NotificationServices;
using NotificationType = UnityEngine.iOS.NotificationType;
#endif

public class VeloxityTemplate : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
#if UNITY_IPHONE && !UNITY_EDITOR
        NotificationServices.RegisterForNotifications(
           NotificationType.Alert |
           NotificationType.Badge |
           NotificationType.Sound);
#endif

#if (UNITY_ANDROID || UNITY_IPHONE) && !UNITY_EDITOR
        Veloxity.Instance.veloxityStart();
#endif
    }

    void Update()
    {
#if UNITY_IPHONE && !UNITY_EDITOR
        if (UnityEngine.iOS.NotificationServices.remoteNotifications.Length > 0)
        {
            if(UnityEngine.iOS.NotificationServices.remoteNotifications[0].userInfo.Contains("vlx")){
                Veloxity.Instance.startBackgroundTransactionWithUserInfo(UnityEngine.iOS.NotificationServices.remoteNotifications[0].userInfo);
                UnityEngine.iOS.NotificationServices.ClearRemoteNotifications();
            }
            else{
                
            }
        }
#endif
    }

    public void vlxAuthorizationDidSucceed()
    {
        Debug.Log("Veloxity AUTH SUCCEED");
    }

    public void vlxAuthorizationDidFailed()
    {
        Debug.Log("Veloxity AUTH FAILED");
    }

    public void vlxAuthorizationDidCompleted()
    {
        Debug.Log("Veloxity AUTH Completed");
    }
}
