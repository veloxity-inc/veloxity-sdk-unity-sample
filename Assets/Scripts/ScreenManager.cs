﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenManager : MonoBehaviour
{
    public void goToSettingsScreen()
    {
        GameObject.Find("MainScreen").transform.localScale = new Vector3(0, 0, 0);
        GameObject.Find("SettingsScreen").transform.localScale = new Vector3(1, 1, 1);
    }

    public void backFromSettingsScreen()
    {
        GameObject.Find("MainScreen").transform.localScale = new Vector3(1, 1, 1);
        GameObject.Find("SettingsScreen").transform.localScale = new Vector3(0, 0, 0);
    }
}
