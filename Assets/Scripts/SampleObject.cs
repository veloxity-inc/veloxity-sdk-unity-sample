﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_IPHONE && !UNITY_EDITOR
using NotificationServices = UnityEngine.iOS.NotificationServices;
using NotificationType = UnityEngine.iOS.NotificationType;
#endif

public class SampleObject : MonoBehaviour
{
	void Start()
	{
		#if UNITY_IPHONE && !UNITY_EDITOR
		NotificationServices.RegisterForNotifications(
		NotificationType.Alert |
		NotificationType.Badge |
		NotificationType.Sound);
		#endif

		GameObject.Find("Toggle").GetComponentInChildren<Toggle>().isOn = Veloxity.Instance.getServiceStatus();
		Veloxity.Instance.veloxityStart();
	}
	void Update()
	{
		#if UNITY_IPHONE && !UNITY_EDITOR
		if (UnityEngine.iOS.NotificationServices.remoteNotifications.Length > 0) {
			if(UnityEngine.iOS.NotificationServices.remoteNotifications[0].userInfo.Contains("vlx")) {
				Veloxity.Instance.startBackgroundTransactionWithUserInfo(UnityEngine.iOS.NotificationServices.remoteNotifications[0].userInfo);
				UnityEngine.iOS.NotificationServices.ClearRemoteNotifications();
			}
			else {

			}
		}
		#endif
	}

	public void authorizationSucceed()
	{
		Debug.Log("authorizationSucceed");
		GameObject.Find("Toggle").GetComponentInChildren<Toggle>().isOn = true;

	}

	public void authorizationFailed()
	{
		Debug.Log("authorizationFailed");
		GameObject.Find("Toggle").GetComponentInChildren<Toggle>().isOn = false;
	}

	public void authorizationCompleted()
	{
		Debug.Log("authorizationCompleted");
	}

	public void toggleClicked()
	{
		bool state = GameObject.Find("Toggle").GetComponentInChildren<Toggle>().isOn;
		if (state)
		{
			Veloxity.Instance.optIn();
		}
		else
		{
			Veloxity.Instance.optOut();
		}
	}
}